package com.rakamin.rakaminguntingbatukertas


class Player(){
    var playerName: String = "Lorem"
     var playerHero: String = "Ipsum"

    fun setName(name: String){
        playerName = name
    }

    fun setHero(hero: String){
        playerHero = hero
    }
}
fun main(){
    println("GAME GUNTING BATU KERTAS")

    val playerOne = Player()
    val playerTwo = Player()

    //set player names
    print("Enter first player name: ")
    val playerOneNameInput = readln()
    print("Enter second player name: ")
    val playerTwoNameInput = readln()

    playerOne.setName(playerOneNameInput)
    playerTwo.setName(playerTwoNameInput)

    //set player heroes
    println("Enter hero for each player")

    print("${playerOne.playerName}: ")
    val playerOneHeroInout = readln()
    playerOne.setHero(playerOneHeroInout)

    print("${playerTwo.playerName}: ")
    val playerTwoHeroInout = readln()
    playerTwo.setHero(playerTwoHeroInout)

//    var result = "Player 2 Menang"


    print("Result: ${compareResult(playerOne, playerTwo)}")
}

fun compareResult(playerOneParam: Player, playerTwoParam: Player) : String {
    if (playerOneParam.playerHero == playerTwoParam.playerHero){
        return "Seri"
    }
    else{
        if ((playerOneParam.playerHero == "Gunting" && playerTwoParam.playerHero == "Kertas") || (playerOneParam.playerHero == "Kertas" && playerTwoParam.playerHero == "Batu") || (playerOneParam.playerHero == "Batu" && playerTwoParam.playerHero == "Gunting")) return "${playerOneParam.playerName} Menang"
        else return "${playerTwoParam.playerName} Menang"
    }
}