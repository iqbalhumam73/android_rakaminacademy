package com.rakamin.rakaminguntingbatukertas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.rakamin.rakaminguntingbatukertas.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var computerHero: String
    private lateinit var result: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.apply {
            btnBatu.setOnClickListener(){
               seeResult("Batu")
            }
            btnGunting.setOnClickListener(){
                seeResult("Gunting")
            }
            btnKertas.setOnClickListener(){
                seeResult("Kertas")
            }
        }
    }

    fun seeResult(userHero: String){
        var computerHeroIndex = (1..3).random()

        when(computerHeroIndex){
            1 -> computerHero = "Gunting"
            2 -> computerHero = "Batu"
            3 -> computerHero = "Kertas"
        }

        result = compareResult(userHero, computerHero)
        binding.tvResult.setText(result)
        setView(userHero, computerHero)
    }

    fun compareResult(userHeroParam: String, computerHeroParam: String) : String {
        if (userHeroParam == computerHeroParam){
            return "Seri"
        }
        else{
            if ((userHeroParam == "Gunting" && computerHeroParam == "Kertas") || (userHeroParam == "Kertas" && computerHeroParam == "Batu") || (userHeroParam == "Batu" && computerHeroParam == "Gunting")) return "Kamu Menang"
            else return "Computer Menang"
        }
    }

    fun setView(userHero: String, computerHero: String){
        binding.tvUserHero.setText(userHero)
        binding.tvComputerHero.setText(computerHero)
    }
}